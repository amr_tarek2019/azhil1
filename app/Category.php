<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categories';
    public function getLogoAttribute($value)
    {
        if ($value) {
            return asset('uploads/categories/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }

    public function setLogoAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/categories/'),$imageName);
            $this->attributes['logo']=$imageName;
        }
    }

    public function requests()
    {
        return $this->hasManyThrough(Request::class, Subcategory::class);
    }

    public function subategories()
    {
        return $this->hasMany('App\Subcategory');
    }
}
