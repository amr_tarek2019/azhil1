<?php

namespace App\Http\Controllers\Api\technician;

use App\Order;
use App\TechnicianOrder;
use App\TechnicianSchedule;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAvailable(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();

        $availableOrders=TechnicianOrder::where('technician_id',$user->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($availableOrders as $res) {
            $res_item['id'] = $res->id;
            $order_data= \App\Order::where('id',$res->order_id)->select('id','order_number')->first();
            $res_item['order_details']=$order_data;
            $res_item['technician_id'] = $res->technician_id;
            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexSchedule(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->first();

        $availableOrders=TechnicianSchedule::where('technician_id',$user->id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($availableOrders as $res) {
            $res_item['id'] = $res->id;
            $order_data= \App\Order::where('id',$res->order_id)->select('id','order_number')->first();
            $res_item['order_details']=$order_data;
            $res_item['technician_id'] = $res->technician_id;
            $res_item['day'] = $res->day;
            $res_item['time'] = $res->time;
            $res_item['date'] = $res->date;
            $res_list[] = $res_item;
        }

        $response = [
            'message' => 'get data of available orders successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
