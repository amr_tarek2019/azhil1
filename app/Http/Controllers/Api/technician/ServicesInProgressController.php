<?php

namespace App\Http\Controllers\Api\technician;

use App\OrderRequest;
use App\ServiceProgress;
use App\Subcategory;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class ServicesInProgressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $lang = ($request->hasHeader('lang')) ? $request->header('lang') : 'en';

        $requests=\App\Request::where('user_id',$request->user_id)->get();
        $res_item = [];
        $res_list  = [];
        foreach ($requests as $res) {
            $res_item['id'] = $res->id;
            $subcategories=Subcategory::where('id',$res->subcategory_id)->select('name_'.$lang.' as name')->first();
            $res_item['subcategory']=$subcategories;
            $res_list[] = $res_item;
        }
        $response = [
            'message' => 'get data of user Requests successfully',
            'status' => 200,
            'data' => $res_list,
        ];
        return \Response::json($response, 200);
        if (!$request->headers->has('jwt')) {
            return response(401, 'check_jwt');
        } elseif (!$request->headers->has('lang')) {
            return response(401, 'check_lang');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $jwt = ($request->hasHeader('jwt')) ? $request->header('jwt') : false;
        $user=User::where('jwt_token',$jwt)->where('user_type','technician')->select('id')->first();
        $validator = Validator::make($request->all(), [
            'service_cost' => 'required',
        ]);
        if ($validator->fails()) {
            $response=[
                'message'=>'failed to booking',
                'status'=>404,
            ];
        }
        foreach ($request->id as $ids)
        {
            $serviceProgress=new ServiceProgress();
            $serviceProgress->technician_id=$user->id;
            $serviceProgress->order_id= $ids;
            $serviceProgress->service_cost=$request->service_cost;
            $serviceProgress->save();
        }
        $response=[
            'message'=>'Reservation request sent successfully',
            'status'=>200,
        ];
        return \Response::json($response,200);
        if (!$request->headers->has('jwt')){
            return response(401, 'check_jwt');
        }elseif (!$request->headers->has('lang')){
            return response(401, 'check_lang');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
