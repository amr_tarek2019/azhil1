<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRate extends Model
{
    protected $table='orders_rate';
    protected $fillable=[ 'user_id', 'technician_id', 'order_id', 'rate', 'feedback'];
}
