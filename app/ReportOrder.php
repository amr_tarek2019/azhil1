<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportOrder extends Model
{
    protected $table='report_orders';
    protected $fillable=['technician_id','price'];
}
