<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table='subcategories';

    public function requests()
    {
        return $this->hasMany('App\Request');
    }

    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/subcategories/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }


    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/subcategories/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
