<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TechnicianOrder extends Model
{
    protected $table='technicians_orders';
    protected $fillable=[ 'technician_id', 'order_id'];
}
