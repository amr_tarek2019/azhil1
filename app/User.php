<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone', 'verify_code',
        'user_status','status','jwt_token',
        'password','image','firebase_token','lat',
        'lng','user_type','facebook_token','google_token','address'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setImageAttribute($value)
    {
        if ($value)
        {
            $imageName=time().'.'.$value->getClientOriginalExtension();
            $value->move(public_path('uploads/user/profile/'),$imageName);
            $this->attributes['image']=$imageName;
        }
    }
    public function getImageAttribute($value)
    {
        if ($value) {
            return asset('uploads/user/profile/'.$value);
        } else {
            return asset('uploads/user/profile/default.png');
        }
    }
    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = bcrypt($password);
    }

    public function requests()
    {
        return $this->hasMany('App\Request');
    }
}
