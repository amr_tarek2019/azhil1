<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('request_id')->unsigned();
            $table->string('address');
            $table->bigInteger('city_id')->unsigned();
            $table->string('lat');
            $table->string('lng');
            $table->string('appartment');
            $table->string('floor');
            $table->string('total_price');
            $table->string('order_number');
            $table->text('additional_info');
            $table->boolean('appointment')->default(0);
            $table->boolean('payment')->default(0);
            $table->boolean('status')->default(0);
            $table->string('month')->default(0);
            $table->string('day')->default(0);
            $table->string('time')->default(0);
            $table->timestamps();

            $table->foreign('request_id')
                ->references('id')->on('requests')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
