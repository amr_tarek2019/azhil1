<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('technical_report_id')->unsigned();
            $table->bigInteger('technician_id')->unsigned();
            $table->string('price');
            $table->timestamps();

            $table->foreign('technician_id')
                ->references('id')->on('technicians')
                ->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('technical_report_id')
                ->references('id')->on('technical_reports')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_orders');
    }
}
