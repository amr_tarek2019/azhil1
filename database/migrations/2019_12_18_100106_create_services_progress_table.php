<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_progress', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('technician_id')->unsigned();
            $table->bigInteger('order_id')->unsigned();
            $table->string('service_cost');
            $table->timestamps();

            $table->foreign('technician_id')
                ->references('id')->on('technicians')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_progress');
    }
}
