@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Create Admin</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Admins</li>
                                <li class="breadcrumb-item active">Create Admin</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        <form class="card" method="POST" action="{{ route('admin.store') }}">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">Create Admin</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">Name</label>
                                            <input class="form-control" name="name" id="name" type="text" placeholder="Name">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">Email</label>
                                            <input class="form-control"  name="email" id="email" type="text" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">phone</label>
                                            <input class="form-control"  name="phone" id="phone" type="text" placeholder="phone">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">Password</label>
                                            <input class="form-control" type="text" placeholder="Password" name="password" id="password">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">submit form</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
