@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Edit Request</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Request</li>
                                <li class="breadcrumb-item active">Edit Request</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-xl-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h5>Order Form Layout</h5>
                                </div>
                                <div class="card-body">
                                    <form class="theme-form"  method="POST" action="{{ route('orders.update',$order->id) }}">
                                        <input hidden name="user_id" value="{{$order->request->user->id}}">
                                       @csrf

                                        <div class="form-group">
                                            <label class="col-form-label pt-0" for="total_price">Total</label>
                                            <input class="form-control" name="total_price" id="total_price" value="{{$order->total_price}}" type="text" aria-describedby="emailHelp" placeholder="Enter email">
                                        </div>
                                        <div class="mb-2">
                                            <div class="col-form-label">Order Status</div>
                                            <select class="form-control form-control-primary btn-square" name="status">
                                                <option value="1">Refused</option>
                                                <option value="2">Accepted</option>
                                            </select>
                                        </div>

                                        <div class="mb-2">
                                            <div class="col-form-label">Order type</div>
                                            <select class="form-control form-control-primary btn-square" name="order_type">
                                                <option value="0">pending </option>
                                                <option value="1">on way</option>
                                                <option value="1">delivered</option>
                                            </select>
                                        </div>
                                        <div class="mb-2">
                                            <div class="col-form-label">Select Technician</div>
                                            <select name="technician" class="js-example-placeholder-multiple col-sm-12" multiple="multiple">
                                                @foreach($technicians as $technician)
                                                    <option value="{{ $technician->id }}">{{ $technician->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div >
                                            <button class="btn btn-primary" type="submit">Submit</button>
                                            <button class="btn btn-secondary">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>






@endsection