@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Orders DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Orders</li>
                                <li class="breadcrumb-item active">Orders Table</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Orders Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Address</th>
                                        <th>city</th>
                                        <th>appointment</th>
                                        <th>payment</th>
                                        <th>status</th>
                                        <th>order number</th>
                                        <th>Created At</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($orders as $key=>$order)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$order->address}}</td>
                                            <td>{{$order->city->name_ar}}</td>
                                            <th>
                                                @if($order->appointment == true)
                                                    <span class="label label-info">appointment</span>
                                                @else
                                                    <span class="label label-danger">not appointment</span>
                                                @endif

                                            </th>
                                            <th>
                                                @if($order->payment == true)
                                                    <span class="label label-info">cash</span>
                                                @else
                                                    <span class="label label-danger">paypal</span>
                                                @endif

                                            </th>

                                            <th>
                                                @if($order->status == null)
                                                    <span class="label label-info">not viewed</span>
                                                    @elseif($order->status == 1)
                                                <span class="label label-success">not accepted</span>
                                                @else
                                                    <span class="label label-danger">accepted</span>
                                                @endif

                                            </th>
                                            <td>{{$order->order_number}}</td>
                                            <td>{{$order->created_at}}</td>
                                            <td>
                                                <a href="{{ route('orders.edit',$order->id) }}" class="btn btn-info">edit</a>
                                                <a href="{{ route('orders.show',$order->id) }}" class="btn btn-air-info">show</a>
                                                <form id="delete-form-{{ $order->id }}" action="{{ route('orders.destroy',$order->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $order->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

