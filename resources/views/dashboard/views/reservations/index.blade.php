@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Reservations DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Reservations</li>
                                <li class="breadcrumb-item active">Reservations Table</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Reservations Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>user</th>
                                        <th>technician</th>
                                        <th>order</th>
                                        <th>order number</th>
                                        <th>status</th>
                                        <th>order type</th>
                                        <th>Created At</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($reservations as $key=>$reservation)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$reservation->user->name}}</td>
                                            <td>{{$reservation->technician->user->name}}</td>
                                            <td>{{$reservation->order->id}}</td>
                                            <td>{{$reservation->order_number}}</td>
                                            <th>@if($reservation->status == 1)
                                                <span class="label label-info">not accepted</span>
                                            @else
                                                <span class="label label-danger">accepted</span>
                                            @endif
                                            </th>
                                            <th>
                                                @if($reservation->order_type == 0)
                                                    <span class="label label-info">pending</span>
                                                @elseif($reservation->order_type == 1)
                                                    <span class="label label-danger">on way</span>
                                                    @else
                                                    <span class="label label-success">delivered</span>
                                                @endif

                                            </th>
                                            <td>{{$reservation->created_at}}</td>
                                            <td style=" width:300px;">
                                                <a href="{{ route('reservations.show',$reservation->id) }}" class="btn btn-info ">Show</a>
                                                <a href="{{route('reservations.invoice.show',$reservation->id)}}" class="btn btn-warning ">Print </a>

                                                <form id="delete-form-{{ $reservation->id }}" action="{{ route('reservations.destroy',$reservation->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger " onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $reservation->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">Delete</button>

                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection

