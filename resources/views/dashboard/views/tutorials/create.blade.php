@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Create Tutorial Slider</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Tutorials</li>
                                <li class="breadcrumb-item active">Create Tutorial Slider</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="edit-profile">
                <div class="row">

                    <div class="col-lg-12">
                        <form class="card" method="POST" action="{{ route('sliders.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="card-header">
                                <h4 class="card-title mb-0">Create Tutorial Slider</h4>
                                <div class="card-options"><a class="card-options-collapse" href="edit-profile.html#" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a><a class="card-options-remove" href="edit-profile.html#" data-toggle="card-remove"><i class="fe fe-x"></i></a></div>
                            </div>
                            <div class="card-body">
                                <div class="row">



                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">title en</label>
                                            <input class="form-control" name="title_en" id="title_en" type="text" placeholder="title en">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">title ar</label>
                                            <input class="form-control"  name="title_ar" id="title_ar" type="text" placeholder="title ar">
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">text en</label>
                                            <textarea class="form-control"  name="text_en" id="text_en" type="text" placeholder="text en"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="form-label">text ar</label>
                                            <textarea class="form-control"  name="text_ar" id="text_ar" type="text" placeholder="text ar"></textarea>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group row">
                                                <label class="form-label" style="margin-left: 30px;">Upload File</label>
                                                <div class="col-sm-9">
                                                    <input class="form-control" id="image" name="image" type="file" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-form-label">App Type Select</div>
                                        <select class="form-control form-control-primary btn-square" name="app_type">
                                            <option value="0">User</option>
                                            <option value="1">Technician</option>

                                        </select>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-form-label">image status Select</div>
                                        <select class="form-control form-control-primary btn-square" name="status">
                                            <option value="0">deactivate</option>
                                            <option value="1">activate</option>

                                        </select>
                                    </div>

                                    <br><br><br><br>




                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <button class="btn btn-primary" type="submit">submit form</button>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>


@endsection
