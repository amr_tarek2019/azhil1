@extends('dashboard.layouts.master')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-header">
                <div class="row">
                    <div class="col">
                        <div class="page-header-left">
                            <h3>Users DataTable</h3>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html"><i data-feather="home"></i></a></li>
                                <li class="breadcrumb-item">Users</li>
                                <li class="breadcrumb-item active">Users Table</li>
                            </ol>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid">
            <div class="row">
                <!-- Zero Configuration  Starts-->
                <div class="col-sm-12">
                    <a href="{{ route('user.create') }}" class="btn btn-primary">Add New</a>
                    @include('dashboard.partials.msg')
                    <div class="card">
                        <div class="card-header">
                            <h5>Users Data</h5>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="display" id="basic-1">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>email</th>
                                        <th>status</th>
                                        <th>Created At</th>
                                        <th>Updated At</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $key=>$user)
                                        <tr>
                                            <td>{{ $key + 1 }}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                            <td>
                                                <div class="media-body text-right icon-state">
                                                <label class="switch">
                                                    <input onchange="updateUserStatus(this)" value="{{ $user->id }}" type="checkbox"
                                                    <?php if($user->user_status == 1) echo "checked";?>>
                                                    <span class="switch-state bg-primary"></span>
                                                </label>
                                                </div>
                                            </td>
                                            <td>{{$user->created_at}}</td>
                                            <td>{{$user->created_at}}</td>
                                            <td>
                                                <a href="{{ route('user.edit',$user->id) }}" class="btn btn-info">edit</a>

                                                <form id="delete-form-{{ $user->id }}" action="{{ route('user.destroy',$user->id) }}" style="display: none;" method="POST">
                                                    @csrf
                                                </form>
                                                <button type="button" class="btn btn-danger" onclick="if(confirm('Are you sure? You want to delete this?')){
                                                        event.preventDefault();
                                                        document.getElementById('delete-form-{{ $user->id }}').submit();
                                                        }else {
                                                        event.preventDefault();
                                                        }">Delete</button>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Zero Configuration  Ends-->
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
@section('myjsfile')
    <script>
        function updateUserStatus(elUser){
            if(elUser.checked){
                var user_status = 1;
            }
            else{
                var user_status = 0;
            }
            $.post('{{ route('user.status',isset($user) ? $user->id : "") }}', {_token:'{{ csrf_token() }}', id:elUser.value, user_status:user_status}, function(data){
                if(data == 1){
                    alert('success , user status updated successfully');
                }
                else{
                    alert('danger , Something went wrong');
                }
            });
        }
    </script>
@endsection